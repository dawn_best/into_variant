use proc_macro2::{Ident, TokenStream};
use quote::quote;

use crate::input::{InputEnum, Variant, VariantBody};

pub fn generate_enum_implementations(input: InputEnum) -> TokenStream {
    let enum_name = input.name();
    let base_implementation = generate_base_implementation(enum_name);
    let variant_implementations = input
        .variants()
        .iter()
        .map(|variant| generate_variant_implementation(enum_name, variant));

    quote! {
        #base_implementation
        #(#variant_implementations)*
    }
}

fn generate_base_implementation(enum_name: &Ident) -> TokenStream {
    quote! {
        impl into_variant::IntoVariant<#enum_name> for #enum_name {
            fn into_variant(self) -> Self {
                self
            }
        }
    }
}

fn generate_variant_implementation(enum_name: &Ident, variant: &Variant) -> TokenStream {
    let variant_name = variant.name();

    let variant_type = match variant.body() {
        VariantBody::Type(ty) => ty,
        VariantBody::None => return quote! {},
    };

    quote! {
        impl<T> into_variant::IntoVariant<T> for #variant_type
        where
            T: into_variant::VariantFrom<#enum_name>,
        {
            fn into_variant(self) -> T {
                T::variant_from(#enum_name::#variant_name(self))
            }
        }
    }
}
