use proc_macro2::{Span, TokenStream};
use quote::format_ident;
use std::fmt::Debug;
use syn::spanned::Spanned;
use syn::{Attribute, Fields, ItemEnum, Meta, NestedMeta, Type};

use crate::input::{InputEnum, Variant, VariantBody};
use crate::parse_enum::InputParseError::{
    HasGenerics, UnexpectedAnnotation, VariantHasComplexType, VariantHasMultipleFields,
    VariantHasNamedFields,
};

#[derive(Clone, Debug)]
pub enum InputParseError {
    ParseError(syn::Error),
    HasGenerics(Span),
    VariantHasNamedFields(Span),
    VariantHasMultipleFields(Span),
    VariantHasComplexType(Span),
    UnexpectedAnnotation(Span),
}

fn get_all_ok_or_errors<T, E: Clone + Debug>(results: Vec<Result<T, E>>) -> Result<Vec<T>, Vec<E>> {
    let errors: Vec<_> = results
        .iter()
        .filter_map(|result| result.as_ref().err().cloned())
        .collect();

    if !errors.is_empty() {
        return Err(errors);
    }

    Ok(results
        .into_iter()
        .map(|result| result.expect("unreachable: checked for errors"))
        .collect())
}

pub fn parse_enum(input: TokenStream) -> Result<InputEnum, Vec<InputParseError>> {
    let parsed =
        syn::parse2::<ItemEnum>(input).map_err(|e| vec![InputParseError::ParseError(e)])?;

    if !parsed.generics.params.is_empty() {
        return Err(vec![HasGenerics(parsed.generics.span())]);
    }

    let variants: Vec<_> = parsed.variants.iter().map(parse_variant).collect();

    let variants = get_all_ok_or_errors(variants)
        .map_err(|errors| errors.into_iter().flatten().collect::<Vec<_>>())?
        .into_iter()
        .filter_map(|option| option)
        .collect();

    Ok(InputEnum::new(parsed.ident, variants))
}

fn parse_variant(variant: &syn::Variant) -> Result<Option<Variant>, Vec<InputParseError>> {
    let annotations: Vec<_> = variant
        .attrs
        .iter()
        .filter(|&attribute| attribute.path.is_ident(&format_ident!("into_variant")))
        .map(parse_annotation)
        .collect();
    let annotations = get_all_ok_or_errors(annotations)?;

    if let Some(Annotation::Skip) = annotations.first() {
        return Ok(None);
    }

    match &variant.fields {
        Fields::Unnamed(fields) => {
            let field_vec: Vec<_> = fields.unnamed.iter().collect();
            if field_vec.is_empty() {
                return Ok(Some(Variant::new(variant.ident.clone(), VariantBody::None)));
            }
            if field_vec.len() > 1 {
                return Err(vec![VariantHasMultipleFields(fields.span())]);
            }

            let ty = match &field_vec[0].ty {
                ty @ Type::Path(_) => ty.clone(),
                _ => return Err(vec![VariantHasComplexType(fields.span())]),
            };

            return Ok(Some(Variant::new(
                variant.ident.clone(),
                VariantBody::Type(ty),
            )));
        }
        Fields::Named(_) => Err(vec![VariantHasNamedFields(variant.span())]),
        Fields::Unit => Ok(Some(Variant::new(variant.ident.clone(), VariantBody::None))),
    }
}

enum Annotation {
    Skip,
}

fn parse_annotation(attribute: &Attribute) -> Result<Annotation, InputParseError> {
    if let Ok(Meta::List(list)) = attribute.parse_meta() {
        let contents: Vec<_> = list.nested.iter().collect();

        if contents.len() != 1 {
            return Err(UnexpectedAnnotation(attribute.span()));
        }

        if let NestedMeta::Meta(meta) = contents[0] {
            if meta.path().is_ident(&format_ident!("skip")) {
                return Ok(Annotation::Skip);
            }
        }
    }

    return Err(UnexpectedAnnotation(attribute.span()));
}
