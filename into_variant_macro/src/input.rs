use proc_macro2::Ident;
use syn::Type;

pub struct InputEnum {
    name: Ident,
    variants: Vec<Variant>,
}

impl InputEnum {
    pub fn new(name: Ident, variants: Vec<Variant>) -> Self {
        Self { name, variants }
    }
    pub fn name(&self) -> &Ident {
        &self.name
    }
    pub fn variants(&self) -> &Vec<Variant> {
        &self.variants
    }
}

pub struct Variant {
    name: Ident,
    body: VariantBody,
}

impl Variant {
    pub fn new(name: Ident, body: VariantBody) -> Self {
        Self { name, body }
    }
    pub fn name(&self) -> &Ident {
        &self.name
    }
    pub fn body(&self) -> &VariantBody {
        &self.body
    }
}

pub enum VariantBody {
    Type(Type),
    None,
}
