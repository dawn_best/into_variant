use proc_macro::TokenStream;

use crate::derive_into_variant::derive_into_variant_inner;

mod derive_into_variant;
mod implement;
mod input;
mod parse_enum;

#[proc_macro_derive(VariantFrom, attributes(into_variant))]
pub fn derive_into_variant(item: TokenStream) -> TokenStream {
    derive_into_variant_inner(item.into()).into()
}
