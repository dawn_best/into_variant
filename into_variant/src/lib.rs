#![doc = include_str!("../Readme.md")]

pub use crate::into_variant::*;

/// Implements conversion from inner types into enum variants.
///
/// Add `#[derive(VariantFrom)]` on an enum with single-field variants. This will implement `IntoVariant<YourEnum>` for all the types contained in the variants, and, by consequence, you'll also get `VariantFrom` on `YourEnum`.
///
/// Example:
/// ```
/// use into_variant::{IntoVariant, VariantFrom};
///
/// #[derive(VariantFrom)]
/// enum Fill {
///     Color(Color),
///     Pattern(Pattern),
/// }
///
/// struct Color {}
/// struct Pattern {}
///
/// // now, this works:
/// let variant = Fill::variant_from(Color {});
/// assert!(matches!(variant, Fill::Color(Color{})));
///
/// // or, alternatively:
/// let variant = Color {}.into_variant();
/// assert!(matches!(variant, Fill::Color(Color{})));
/// ```
///
/// If you don't want to implement `IntoVariant` for a particular variant's contents, you can skip it with the `into_variant(skip)` attribute:
///
/// ```
/// use into_variant::{IntoVariant, VariantFrom};
///
/// #[derive(VariantFrom)]
/// enum Fill {
///     Color(Color),
///     #[into_variant(skip)]
///     Pattern(Pattern),
/// }
///
/// struct Color {}
/// struct Pattern {}
///
/// // now, this works:
/// let variant = Fill::variant_from(Color {});
/// // but this doesn't:
/// // let variant = Fill::variant_from(Pattern {});
/// ```
///
/// Variants with nothing in them (empty tuple or unit variants) are automatically skipped:
/// ```
/// use into_variant::VariantFrom;
///
/// #[derive(VariantFrom)]
/// enum Fill {
///     // nothing will be generated for these 2
///     Color(),
///     Pattern,
/// }
/// ```
///
/// `derive(VariantFrom)` only works on variants with a single, simple field (struct or enum), like `Color(Color)`. It does not work for more complex values, such as tuples or arrays. They must be explicitly skipped with `into_variant(skip)` or you'll get a compile-time error.
pub use into_variant_macro::VariantFrom;

mod into_variant;

#[test]
fn test_compile_errors() {
    let t = trybuild::TestCases::new();
    t.compile_fail("tests_error/*.rs");
}
