use into_variant::VariantFrom;

fn main() {}

#[derive(VariantFrom)]
struct Struct {
    value: u32,
}
