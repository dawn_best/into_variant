use into_variant::VariantFrom;

fn main() {}

#[derive(VariantFrom)]
enum MessedUp<T> {
    Foo(T),
}
