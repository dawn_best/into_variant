use into_variant::VariantFrom;

#[derive(VariantFrom)]
enum Fill {
    Color((Color, Color)),
    Pattern(Pattern),
}

struct Color {}

struct Pattern {}

fn main() {}
