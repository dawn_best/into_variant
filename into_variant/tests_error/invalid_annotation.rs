use into_variant::VariantFrom;

#[derive(VariantFrom)]
#[allow(dead_code)]
enum Enum1 {
    #[into_variant]
    A,
    B,
}

#[derive(VariantFrom)]
#[allow(dead_code)]
enum Enum2 {
    #[into_variant(Skip)]
    A,
    B,
}

#[derive(VariantFrom)]
#[allow(dead_code)]
enum Enum3 {
    #[into_variant()]
    A,
    B,
}

fn main() {}
