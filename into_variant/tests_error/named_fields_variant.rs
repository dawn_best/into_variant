use into_variant::VariantFrom;

fn main() {}

#[derive(VariantFrom)]
enum MessedUp {
    Foo { foo: Foo },
}

struct Foo {}
