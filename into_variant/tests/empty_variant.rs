use into_variant::VariantFrom;

#[derive(VariantFrom)]
#[allow(dead_code)]
enum AppMessage {
    Quit,
    EmptyMessage(),
    EditorMessage(EditorMessage),
}

struct EditorMessage {}

#[test]
fn test_from_editor_message() {
    assert!(matches!(
        AppMessage::variant_from(EditorMessage {}),
        AppMessage::EditorMessage(_)
    ))
}
