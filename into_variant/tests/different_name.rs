use into_variant::VariantFrom;

struct UserDetails {}

struct AdminDetails {}

#[derive(VariantFrom)]
enum Account {
    User(UserDetails),
    Admin(AdminDetails),
}

#[test]
fn test_from_user() {
    assert!(matches!(
        Account::variant_from(UserDetails {}),
        Account::User(_)
    ))
}

#[test]
fn test_from_admin() {
    assert!(matches!(
        Account::variant_from(AdminDetails {}),
        Account::Admin(_)
    ))
}
