use into_variant::VariantFrom;

#[derive(VariantFrom)]
enum Animal {
    Dino(Dino),
    Bird(Bird),
}

struct Dino {}

#[test]
fn test_from_dino() {
    assert!(matches!(Animal::variant_from(Dino {}), Animal::Dino(_)))
}

#[derive(VariantFrom)]
enum Bird {
    Eagle(Eagle),
    Parrot(Parrot),
}

struct Eagle {}

#[test]
fn test_from_eagle() {
    assert!(matches!(
        Animal::variant_from(Eagle {}),
        Animal::Bird(Bird::Eagle(_))
    ))
}

#[derive(VariantFrom)]
enum Parrot {
    RedParrot(RedParrot),
    BlueParrot(BlueParrot),
}

struct RedParrot {}

#[test]
fn test_from_red_parrot() {
    assert!(matches!(
        Animal::variant_from(RedParrot {}),
        Animal::Bird(Bird::Parrot(Parrot::RedParrot(_)))
    ))
}

#[derive(VariantFrom)]
enum BlueParrot {
    Polly(Polly),
    Rio(Rio),
}

struct Polly {}

struct Rio {}

#[test]
fn test_from_polly() {
    assert!(matches!(
        Animal::variant_from(Polly {}),
        Animal::Bird(Bird::Parrot(Parrot::BlueParrot(BlueParrot::Polly(_))))
    ))
}

#[test]
fn test_from_rio() {
    assert!(matches!(
        Animal::variant_from(Rio {}),
        Animal::Bird(Bird::Parrot(Parrot::BlueParrot(BlueParrot::Rio(_))))
    ))
}
