use into_variant::VariantFrom;

#[derive(VariantFrom)]
#[allow(dead_code)]
enum Fill {
    Color(Color),
    #[into_variant(skip)]
    Gradient((Color, Color)),
}

struct Color {}

#[test]
fn test_from_color() {
    assert!(matches!(Fill::variant_from(Color {}), Fill::Color(_)))
}
