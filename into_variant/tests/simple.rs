use into_variant::VariantFrom;

#[derive(VariantFrom)]
enum Fill {
    Color(Color),
    Pattern(Pattern),
}

struct Color {}

struct Pattern {}

#[test]
fn test_from_color() {
    assert!(matches!(Fill::variant_from(Color {}), Fill::Color(_)))
}

#[test]
fn test_from_pattern() {
    assert!(matches!(Fill::variant_from(Pattern {}), Fill::Pattern(_)))
}
