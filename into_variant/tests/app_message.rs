use into_variant::{IntoVariant, VariantFrom};

#[derive(VariantFrom)]
enum AppMessage {
    File(FileMessage),
    Editor(EditorMessage),
}

#[derive(VariantFrom)]
enum FileMessage {
    Save(SaveMessage),
    Close(CloseMessage),
}

#[derive(VariantFrom)]
enum EditorMessage {
    Insert(InsertMessage),
    Formatting(FormattingMessage),
}

struct SaveMessage {}
struct CloseMessage {}
struct InsertMessage {}

#[test]
fn test_variant_from() {
    assert!(matches!(
        AppMessage::variant_from(SaveMessage {}),
        AppMessage::File(FileMessage::Save(SaveMessage {}))
    ));

    assert!(matches!(
        AppMessage::variant_from(InsertMessage {}),
        AppMessage::Editor(EditorMessage::Insert(InsertMessage {}))
    ));
}

#[test]
fn test_into_variant() {
    function_that_takes_app_message(SaveMessage {}.into_variant())
}

fn function_that_takes_app_message(_message: AppMessage) {}

#[derive(VariantFrom)]
enum FormattingMessage {
    ClearFormatting(ClearFormattingMessage),
}

struct ClearFormattingMessage {}

#[test]
fn test_deeper_nesting() {
    assert!(matches!(
        AppMessage::variant_from(ClearFormattingMessage {}),
        AppMessage::Editor(EditorMessage::Formatting(
            FormattingMessage::ClearFormatting(ClearFormattingMessage {})
        ))
    ))
}
